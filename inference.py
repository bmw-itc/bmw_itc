"""
Holds the functionality of Detection Inference given test images, frozen_inference_graph and labels map

Credits:
    https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb

    Removed model download

Usage:

# From bmw_itc repository
    ### Seperate by space! ###
  python inference.py --labels_map_path=data/labelmap.pbtxt \
                      --model_ckpt_path=path\to\model\checkpoint - frozen_inference_graph.pb
                      --number_of_classes=n
                      --test_image_dir=path\to\test\image\dir \
                      --output_path=path\to\outputs

"""


import os
import sys
import glob
import time
import tarfile
import zipfile
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import six.moves.urllib as urllib

from PIL import Image
from io import StringIO
from collections import defaultdict

from models.research.object_detection.utils import label_map_util
from models.research.object_detection.utils import visualization_utils as vis_util
from models.research.object_detection.utils import ops as utils_ops

if tf.__version__ < '1.4.0':
    raise ImportError('Please upgrade your tensorflow installation to v1.4.* or later!')

flags = tf.app.flags
flags.DEFINE_string('labels_map_path', '', 'path to the labels_map.pbtxt')
flags.DEFINE_string('model_ckpt_path', '', 'Path to the models checkpoint')
flags.DEFINE_string('number_of_classes', '', 'Number of classes')
flags.DEFINE_string('test_images_dir', '', 'Path to test images dir')
flags.DEFINE_string('output_path', '', 'Path to output images dir')

FLAGS = flags.FLAGS


def load_label_map(path_to_labels, num_classes):
    # Load label map
    label_map = label_map_util.load_labelmap(path_to_labels)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=num_classes,
                                                                use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    return label_map, categories, category_index


def load_image_into_numpy_array(image):
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)


def run_inference_for_single_image(image, graph):

    with graph.as_default():
        with tf.Session() as sess:
            # Get handles to input and output tensors
            ops = tf.get_default_graph().get_operations()
            all_tensor_names = {output.name for op in ops for output in op.outputs}
            tensor_dict = {}
            for key in ['num_detections', 'detection_boxes', 'detection_scores',
                  'detection_classes', 'detection_masks']:
                tensor_name = key + ':0'
                if tensor_name in all_tensor_names:
                    tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(
                      tensor_name)
            if 'detection_masks' in tensor_dict:
                # The following processing is only for single image
                detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
                detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])

                # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
                real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
                detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
                detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
                detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
                    detection_masks, detection_boxes, image.shape[0], image.shape[1])
                detection_masks_reframed = tf.cast(
                    tf.greater(detection_masks_reframed, 0.5), tf.uint8)

                # Follow the convention by adding back the batch dimension
                tensor_dict['detection_masks'] = tf.expand_dims(
                    detection_masks_reframed, 0)
            image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

            # Run inference
            output_dict = sess.run(tensor_dict,
                                 feed_dict={image_tensor: np.expand_dims(image, 0)})

            # all outputs are float32 numpy arrays, so convert types as appropriate
            output_dict['num_detections'] = int(output_dict['num_detections'][0])
            output_dict['detection_classes'] = output_dict[
                  'detection_classes'][0].astype(np.uint8)
            output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
            output_dict['detection_scores'] = output_dict['detection_scores'][0]
            if 'detection_masks' in output_dict:
                output_dict['detection_masks'] = output_dict['detection_masks'][0]
    return output_dict


def run_inference(test_image_path, detection_graph, category_index, image_size):

    print("======= Starting Inference =======")
    for image_index, image_path in enumerate(test_image_path):
        start_time = time.time()
        image = Image.open(image_path)
        # the array based representation of the image will be used later in order to prepare the
        # result image with boxes and labels on it.
        image_np = load_image_into_numpy_array(image)
        # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
        image_np_expanded = np.expand_dims(image_np, axis=0)
        # Actual detection.
        output_dict = run_inference_for_single_image(image_np, detection_graph)

        # Visualization of the results of a detection.
        vis_util.visualize_boxes_and_labels_on_image_array(
            image_np,
            output_dict['detection_boxes'],
            output_dict['detection_classes'],
            output_dict['detection_scores'],
            category_index,
            instance_masks=output_dict.get('detection_masks'),
            use_normalized_coordinates=True,
            line_thickness=8)

        plt.figure(figsize=image_size)
        plt.imshow(image_np)
        file_name = os.path.basename(image_path).split('.')[0]
        plt.savefig(FLAGS.output_path + file_name)
        time_elapse = time.time() - start_time
        print('Inference Image {}/{}, time elapse = {:.4f}'.format(image_index, len(test_image_path), time_elapse))


def main():

    # Load tensorflow model into memory
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(FLAGS.model_ckpt_path, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

    label_map, categories, category_index = load_label_map(FLAGS.labels_map_path, int(FLAGS.number_of_classes))
    test_image_paths = glob.glob(FLAGS.test_images_dir +'/*.jpg')
    IMAGE_SIZE = (12, 8)      # Size, in inches, of the output images.
    run_inference(test_image_paths, detection_graph, category_index, IMAGE_SIZE)


if __name__ == '__main__':
    main()
