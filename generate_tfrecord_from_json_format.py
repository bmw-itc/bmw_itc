"""
Holds the functionality of conversion from COCO *.json annotation format to Tf-record


Credits:
    Generate Tf-record core :
    https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/using_your_own_dataset.md

    Added:
        * Extraction of the relevant annotation information from the COCO annotation file

Usage:
  # From tensorflow/models/
  # Create train data:
  python generate_tfrecord.py --json_input=data/train_labels.csv  --output_path=train.record --data_path=data/train/
  # Create test data:
  python generate_tfrecord.py --json_input=data/test_labels.csv  --output_path=test.record --data_path=data/test/

"""

import os
import json
import numpy as np
import tensorflow as tf

from models.research.object_detection.utils import dataset_util

flags = tf.app.flags
flags.DEFINE_string('json_input', '', 'path to the json annotation')
flags.DEFINE_string('output_path', '', 'Path to output TFRecord')
flags.DEFINE_string('data_path', '', 'Path to data')

FLAGS = flags.FLAGS


def load_image(image_path):
    with open(image_path, "rb") as imageFile:
        encoded_image_data = imageFile.read()

    return encoded_image_data


def get_normalized_xy_mins_maxs_from_single_bbox(bbox, image_size):
    xmin, ymin, width, height = bbox
    xmax = (xmin + width) / image_size[1]
    ymax = (ymin + height) / image_size[0]

    xmin /= image_size[1]
    ymin /= image_size[0]

    return [xmin, xmax, ymin, ymax]


def get_normalized_xy_mins_maxs_for_all_bbox(bboxs, image_size):
    xy_mins_maxs_normalized = []
    for bbox in bboxs:
        xy_mins_maxs_normalized.append(get_normalized_xy_mins_maxs_from_single_bbox(bbox, image_size))
    return np.array(xy_mins_maxs_normalized)


def get_image_annotation(image_path, annotation_file):
    file_name = os.path.basename(image_path)
    image_format = os.path.basename(image_path).split('.')[1].encode()
    image_id = int(file_name.split('.')[0].lstrip('0'))
    bboxs = [annotation['bbox'] for annotation in annotation_file['annotations'] if annotation['image_id'] == image_id]
    category_ids = [annotation['category_id'] for annotation in annotation_file['annotations'] if
                    annotation['image_id'] == image_id]

    category_names = [category['name'] for category in annotation_file['categories'] if category['id'] in category_ids]
    category_names = [cat.encode() for cat in category_names]

    image_size = [annotation['segmentation']['size'] for annotation in annotation_file['annotations'] if
                  annotation['image_id'] == image_id][0]

    xy_mins_maxs = get_normalized_xy_mins_maxs_for_all_bbox(bboxs, image_size)
    encoded_image_data = load_image(image_path)

    keys = ['height', 'width', 'file_name', 'encoded_image_data', 'image_format',
            'xmins', 'xmaxs', 'ymins', 'ymaxs', 'classes_text', 'classes']
    values = [image_size[1], image_size[0], file_name.encode(), encoded_image_data, image_format, xy_mins_maxs[:, 0],
              xy_mins_maxs[:, 1], xy_mins_maxs[:, 2], xy_mins_maxs[:, 3], category_names, category_ids]

    image_annotation = dict(zip(keys, values))

    return image_annotation


def list_images_in_path(input_path):
    image_paths = []
    for file in os.listdir(input_path):
        if file.endswith(".jpg") or file.endswith(".png"):
            image_paths.append(os.path.join(input_path,file))
    return image_paths


def create_tf_example(image_annotation):
    height = image_annotation['height']
    width = image_annotation['width']
    filename = image_annotation['file_name']  # Filename of the image. Empty if image is not from file
    encoded_image_data = image_annotation['encoded_image_data']  # Encoded image bytes
    image_format = image_annotation['image_format']  # b'jpeg' or b'png'

    # box coordinates are floats measured from the top left image corner

    xmins = image_annotation['xmins']  # List of normalized left x coordinates in bounding box (1 per box)
    xmaxs = image_annotation['xmaxs']  # List of normalized right x coordinates in bounding box
    # (1 per box)
    ymins = image_annotation['ymins']  # List of normalized top y coordinates in bounding box (1 per box)
    ymaxs = image_annotation['ymaxs']  # List of normalized bottom y coordinates in bounding box
    # (1 per box)
    classes_text = (image_annotation['classes_text'])  # List of string class name of bounding box (1 per box)

    classes = np.array(image_annotation['classes']).tostring() # List of integer class id of bounding box (1 per box)

    tf_example = tf.train.Example(features=tf.train.Features(feature={
        'image/height': dataset_util.int64_feature(height),
        'image/width': dataset_util.int64_feature(width),
        'image/filename': dataset_util.bytes_feature(filename),
        'image/source_id': dataset_util.bytes_feature(filename),
        'image/encoded': dataset_util.bytes_feature(encoded_image_data),
        'image/format': dataset_util.bytes_feature(image_format),
        'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
        'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
        'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
        'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
        'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
        'image/object/class/label': dataset_util.int64_list_feature(classes),
    }))
    return tf_example


def generate_tfrecord_from_json(_):
    writer = tf.python_io.TFRecordWriter(FLAGS.output_path)

    with open(FLAGS.json_input) as annotation_file:
        annotation = json.load(annotation_file)

    image_paths = list_images_in_path(FLAGS.data_path)

    for image_path in image_paths:
        image_annotation = get_image_annotation(image_path, annotation)
        tf_example = create_tf_example(image_annotation)
        writer.write(tf_example.SerializeToString())

    writer.close()


if __name__ == '__generate_tfrecord_from_json__':
    tf.app.run()
